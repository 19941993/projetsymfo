<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Projet;
use App\Repository\ProjetRepository;

class HomeController extends AbstractController

{
/**
     * @Route("/home/projets", name="index")
     */
     //fonction qui me retourne ma page
     //injection des dependences :quand il va appeller la fonction index il va savoir que cette fonction à besoin d'une instance de
     //la classe ArticleRepository qui s'appellera repo ainsi je peux mettre la troisieme ligne en commentaire
    public function index(ProjetRepository $repo)
    {//discuter avec doctrine
   // $repo=$this->getDoctrine()->getRepository(Projet::class);
    $projets=$repo->findAll();
    //pas besoin de preciser le dossier template car symfo sais que tous ses dossiers sont ici
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            //Passer à twig mes projets pour me les afficher
            'projets' => $projets
        ]);
    }

    /**
    *@Route("/" , name="acceuil")
    */

    public function home(){
    //je lui donne le fichier twig à afficher ()
    return $this->render('home/acceuil.html.twig');
    }
     /**
        *@Route("/home/cv" , name="cv")
        */
     public function cv(){
        //je lui donne le fichier twig à afficher ()
        return $this->render('home/cv.html.twig');
        }
        //concept de route paramettré
         /**
          *@Route("home/projet/{id}",name="projet")
          */
            public function show (ProjetRepository $repo,$id){
           // $repo = $this->getDoctrine()->getRepository(Projet::class);
            //trouve moi un proejt dont l'id comme envoyé dans l'url
            $projet=$repo->find($id);
            return $this->render('home/projet.html.twig',['projet'=>$projet ]);
         }
         //grace à l'injection des dependences on a des fonctioons plus courte et on saucie pas de l'inetion des dep



}
