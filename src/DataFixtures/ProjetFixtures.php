<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Projet;

class ProjetFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    for ($i=1;$i<=5;$i++){
        $projet =new Projet();
        $projet->setIntitule("petite Annonce")
               ->setDescription(" site de petites annonces ecris en html,css,javascript")
               ->setLien("https://gitlab.com/19941993/monprojetspringmvc")
               ->setCreatedAt(new \DateTime());
               //deamander à manager de faire persister mon projet
               $manager->persist($projet);
        // $product = new Product();
        // $manager->persist($product);

}
//pour que le projet existe realement dans ma base
        $manager->flush();
    }
}
